

class User {
    constructor(data) {
        this.id = data.id;
        this.name = data.name;
        this.email = data.email;
        this.address = data.address;
        this.phone = data.phone;

    }

    get info() {
        return data;
    }

    set info(newdata) {
        this.id = newdata.id;
        this.name = newdata.name;
        this.email = newdata.email;
        this.address = newdata.address;
        this.phone = newdata.phone;
    }
}

let bob = new User ({id: '01', name: 'bob', email: 'bobx@gmail.com', address: 'Minsk', phone: '+37529111111'});

console.log(bob);

